// create an array

let myArr = [1,2,3,4,5,6];

console.log(myArr.length); // 6

console.log(myArr[0]); // 1


// loop over an array

myArr.forEach((item, index) =>{
    console.log(index, item);
})

// add to the end of an array

myArr.push('7');

// remove from the end of an array

myArr.pop();

// add to the front of the array

myArr.unshift(0);

// remove from the front of the array

myArr.shift();

// find the index of an item in the array

myArr.indexOf(2);

// remove an item by index postion

myArr.splice(pos, 3);

// copy an array

let myArrCopy = myArr.slice();

