// creating a map object
let myMap = new Map();

let key_string = 'a string';
let keyObj = {};
let keyFunc = function(){
    return "It is a function";
};

//setting the values to map

myMap.set(key_string, "value associated with 'a string'");
myMap.set(keyObj, "value associated with keyObj");
myMap.set(keyFunc, "value associated with keyFunc");

// Size of the map
myMap.size
// getting value against the keys
myMap.get(key_string);// "value associated with 'a string'";
myMap.get(keyObj); // "value associated with a keyObj";
console.log(myMap.get(keyFunc));// "value associated with a function";

myMap.get('a string'); // value associated with 'a string' because keyString == 'a string'

myMap.get({}); // undefined, because keyObj! == {}
myMap.get(function(){}) // undefine, because keyFunc !== function(){}


myMap.set(NaN, 'Not a number');
myMap.get(NaN); // "Not a number"

let otherNaN = Number("foo");
myMap.get(otherNaN); // "not a number";


// iterating map with for 

for(let [key, value] of myMap){
    console.log(key + " " + value);
}
// only keys
for(let key of myMap.keys()){
    console.log(key);
}
// only values
for(let value of myMap.values()){
    console.log(value);
}

// map entries() = return an array of [key, values]

for(let [key, value] of myMap.entries()){
    console.log(key + ' = ' + value);
}

// iterating map with forEach()

myMap.forEach(function(value, key){
    console.log(key + " " + value);
});

// delete a specific element using key

myMap.delete(key_string);// remove all key-valye pairs from the myMap object


// find out if there is value are there associated with key

myMap.has(key_string) // return true if there is a valye associated with key_string else false;

// Remove all value from the map

// myMap.clear();

console.log(myMap.size)

// we can merge and clone of maps

let map1 = new Map([[1,"one"]]);
let map2 = new Map([[2, "Two"]]);

let clone = new Map(map1);
console.log(clone.get(1));

let merged = new Map([...map1, ...map2]);
console.log(merged.get(1));
console.log(merged.get(2));

