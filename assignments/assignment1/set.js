// using set objects

let mySet = new Set();
//adding value to set
mySet.add(1);
mySet.add(2);
mySet.add(2);// it will won't add because 2 is already present in set and set won't contain duplicate values.

mySet.add("some text"); // Set[1,2,'some text'];// Set is a collection of a hetrogenous group of elements.
let o = {a:1, b:2};
mySet.add(o);

mySet.add({a:1,b:2});

mySet.has(1); // return true because 1 is a element of a set;
mySet.has(3); // return false because 3 is not a element of a set.

mySet.add(3);
mySet.has(Math.sqrt(9)); // return true because 3 is a element of a set;
mySet.has('Some TEXT'.toLowerCase()); // return true because 'some text' present in set;
 

mySet.has(o) // return true;

// remove element from the set

mySet.delete(2); // if 2 is present in the set it will remove that element from the set;

// size of the set

mySet.size; // return the size of the set;

console.log(mySet); // Set { 1, 'some text', { a: 1, b: 2 }, { a: 1, b: 2 }, 3 }

// Iterating sets

for(let item of mySet){
    console.log(item);
}

// by keys
for(let item of mySet.keys()){
    console.log(item);
}

// key and values

for(let [key, value] of mySet.entries()){
    console.log(key + " " + value);
}

// convert set object to an array object

let myArr = Array.from(mySet);

console.log(myArr);

let mySet2 = new Set([1,2,3,4,5]);

// Intersecting between sets

let intersect = new Set([...mySet].filter(x => mySet2.has(x)))

console.log(intersect);

// difference between sets

let difference = new Set([...mySet].filter(x => !mySet2.has(x)))

console.log(difference);

// Iterate set entries with forEach()

mySet2.forEach(value =>{
    console.log(value);
})

// Implementing basic set operations

function isSuperset(mySet, mySet2){
    for(let elem of mySet2){
        if(!mySet.has(elem)){
            return false;
        }
    }
    return true;
}

function union(mySet, mySet2){
    let _union = new Set(mySet)
    for(let elem of mySet2){
        _union.add(elem);
    }
    return _union;
}

function intersection(mySet, mySet2){
    let _intersection = new Set();
    for(let elem of mySet2){
        if(mySet.has(elem)){
            _intersection.add(elem);
        }
    }
    return _intersection;
}

isSuperset(mySet, mySet2);
union(mySet, mySet2);
intersection(mySet, mySet2);
